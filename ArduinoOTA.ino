/*
 * ota.cpp
 *
 * Created: 27/07/2018 15:45:10
 *  Author: Collo
 */ 


// TODO	-Better error handling

//States when parsing the hex file
#define HEX_START_LINE						0
#define HEX_GET_BYTE_COUNT					1
#define HEX_GET_ADDRESS						2
#define HEX_GET_RECORD_TYPE					3
#define HEX_GET_DATA						4
#define HEX_GET_CHECKSUM					5
#define HEX_END_OF_LINE						6

#define HEX_BUFFER_SIZE						64


//STK500V2 compatible command set
#define MSG_START							0x1B
#define TOKEN								0x0E
#define CMD_LOAD_ADDRESS                    0x06
#define CMD_ENTER_PROGMODE_ISP              0x10
#define CMD_LEAVE_PROGMODE_ISP              0x11
#define CMD_PROGRAM_FLASH_ISP               0x13


#define HTTP_BYTE_RANGE						4096

//#define GPRS_APN      "bew.orange.co.ke"
#define GPRS_APN      "safaricom"
#define GPRS_USER     "saf"
#define GPRS_PASS     "data"

#include "ota.h"


#define SerialAT Serial1

void init_eeprom();
int write_string_to_eeprom(uint16_t, uint8_t*, int);
int write_to_eeprom(uint32_t, uint8_t);

void write_to_hex_buffer(uint8_t *);
uint8_t read_from_hex_buffer(uint8_t*);
uint8_t get_hex_4(uint8_t);
int parse_hex_data(uint8_t);// return parsed data, set message state
uint8_t start_stk_msg(int, int, int, uint8_t*);
uint8_t write_stk_msg_to_eeprom();


void ota_handler();
void connect_to_ota_http_server();
int send_ota_http_request();
uint8_t download_byte();
void write_firmware_ver_to_eeprom(uint8_t *);
void read_firmware_ver_from_eeprom(uint8_t *);
uint8_t new_firmware_available(uint8_t *);
uint8_t get_server_firmware_ver(uint8_t *);
uint8_t get_download_byte_chunks(uint32_t, uint16_t);

//************************************************************
//The code is meant to work with the existing arduino bootloader to allow for firmware flash OTA or via an upload from arduino IDE
//It uses the already existing STK500 commandset available on the STK500V2 bootloader.
//Only parses hex files. This provides good enough error checking at the same time allowing easier downloads of large files.
//A flag on the internal EEPROM is used to indicate that firmware is ready to flash.
//The flag is reset on successful flash and checking this flag on boot can indicate a successful or unsuccessful flash.
//
//***
//Serial flashing via arduino IDE does not work on the PCBA, Bootloader needs to be updated to remove serial flash 
//*************************************************************

const char otaServer[] = "";// Firmware update server

const char otaResource[] = "/firmware.hex";

uint32_t fileSize = 5062; //default length

TinyGsm modem(SerialAT);
TinyGsmClient client(modem);


void setup()
{
	_delay_ms(200);
		
	Serial.begin(115200);
	SerialAT.begin(115200);
	
	Serial.println("Initialising");
	
	start_comms();
	
	ota_handler();
}

void loop()
{
	
}

bool start_comms()
{
	DEBUG_PRINT("START COMMUNICATIONS");
	wdt_disable();
	modem.restart();
	
	DEBUG_PRINT("[OK] MODEM");
	while (!gprs_connect()) {
		DEBUG_PRINT("Error on GPRS connection");
	}

	//IMEI = modem.getIMEI();
	//IMEI = "867856031707505";
	//phoneNumber=modem.getSubscriberNumber();
	
	//DEBUG_PRINT(IMEI);
	//DEBUG_PRINT(phoneNumber);

	delay(1000);
	wdt_enable(WDTO_8S);
}

int gprsConnectionAttempts=0;
bool gprs_connect(){
	while(SerialAT.available()>0){
		Serial.println(SerialAT.read());
	}
	
	DEBUG_PRINT("[WAIT] NETWORK");
	while (!modem.waitForNetwork()) {
		DEBUG_PRINT("[WAIT]");
		delay(250);
	}
	DEBUG_PRINT("[OK] NETWORK");
	
	DEBUG_PRINT("[WAIT] GPRS");
	wdt_reset();
	while (!modem.gprsConnect(GPRS_APN, GPRS_USER, GPRS_PASS)) {
		if(gprsConnectionAttempts++ > 5){
			DEBUG_PRINT("REBOOT");
			//write_state_to_eeprom(cdUserFunds);
			wdt_enable(WDTO_8S);
			while(1)
			;
		}
		delay(1000);
	}
	gprsConnectionAttempts=0;
	wdt_reset();
	DEBUG_PRINT("[OK] GPRS");
	
	return true;
}

//Parses hex file and writes to EEPROM. Returns 0 if successful
uint8_t write_stk_msg_to_eeprom()
{
	uint32_t _address = 0;
	
	uint8_t hexParseState = HEX_START_LINE;
	uint16_t hexByteCount = 0;
	uint16_t hexAddress = 0;
	uint8_t hexRecordType = 0;
	uint8_t hexChecksum = 0;
	uint8_t calculatedHexChecksum = 0;
	
	uint8_t hexData[30];
	uint8_t writeToEEPROM = 0;
	
	uint8_t inProgMode = 0;
	uint8_t stkSequenceNumber = 0;
	uint8_t stkMSGState = 0;
	uint8_t stkChecksum = 0;
	
	//Serial.println("WRITING TO EEPROM");
	
	while(1){
		switch(hexParseState){
			case HEX_START_LINE:{
				calculatedHexChecksum = 0;
				
				if(download_byte() == 0x3A){
					hexParseState = HEX_GET_BYTE_COUNT;
				}
				break;
			}
			
			case HEX_GET_BYTE_COUNT:{
				hexByteCount = 0;
				
 				hexByteCount |= (get_hex_4(download_byte()) << 4);
 				hexByteCount |= get_hex_4(download_byte());
				 
				calculatedHexChecksum += hexByteCount;

				hexParseState = HEX_GET_ADDRESS;
				break; 
			}
			
			case HEX_GET_ADDRESS:{
				hexAddress = 0;
				
				hexAddress |= (get_hex_4(download_byte()) << 12);
				hexAddress |= (get_hex_4(download_byte()) << 8);
				hexAddress |= (get_hex_4(download_byte()) << 4);
				hexAddress |= (get_hex_4(download_byte()));
				
				calculatedHexChecksum += (hexAddress&0xFF);
				calculatedHexChecksum += ((hexAddress>>8)&0xFF);
				
				//DEBUG_PRINT("Address: " + String(hexAddress));
				
				hexParseState = HEX_GET_RECORD_TYPE;
				break;
			}
			
			case HEX_GET_RECORD_TYPE:{
				hexRecordType = 0;
				
				hexRecordType |= (get_hex_4(download_byte()) << 4);
				hexRecordType |= (get_hex_4(download_byte()));
				
				calculatedHexChecksum += hexRecordType;
				
				if(hexRecordType == 0x00){
					hexParseState = HEX_GET_DATA;
				}
				else if(hexRecordType == 0x01){
					writeToEEPROM = 1;
					stkMSGState = CMD_LEAVE_PROGMODE_ISP;	
					continue;
				}
				break;
			}
			
			case HEX_GET_DATA:{
				uint8_t tempCount = hexByteCount;
				
				for(int i=0; i<tempCount; ++i){
					uint8_t tempVal = 0;
					tempVal |= (get_hex_4(download_byte()) << 4);
					tempVal |= (get_hex_4(download_byte()));
					
					calculatedHexChecksum += tempVal;
					
					hexData[i] = tempVal;
				}
				
				hexParseState = HEX_GET_CHECKSUM;
				break;
			}
			
			case HEX_GET_CHECKSUM:{
				hexChecksum = 0;
				
				hexChecksum |= (get_hex_4(download_byte()) << 4);
				hexChecksum |= (get_hex_4(download_byte()));
				
				hexParseState = HEX_END_OF_LINE;
				stkMSGState = CMD_LOAD_ADDRESS;
				break;
			}
			
			case HEX_END_OF_LINE:{
				calculatedHexChecksum &= 0xFF;
				calculatedHexChecksum = (~calculatedHexChecksum)+1;
				
				if(calculatedHexChecksum == hexChecksum){
					hexParseState = 7;
					writeToEEPROM = 1;//Hex Line parsed, ready to write to EEPROM
					break;
				}
				else{
					DEBUG_PRINT("Checksum Error");
					return 1;
				}
			}
		}
		
		if(writeToEEPROM == 1){
			switch(stkMSGState){
				case CMD_LOAD_ADDRESS:{
					if(inProgMode == 1){
						stkMSGState = CMD_PROGRAM_FLASH_ISP;
						break;
					}
					DEBUG_PRINT("Load Address..");
					stkChecksum = 0;
					
					uint16_t messageSize = 5;
					if(start_stk_msg(_address, stkSequenceNumber, messageSize, &stkChecksum)){
						Serial.println("Write Data Fail");
						return 1;
					}
					_address += 5;
					
					stkChecksum ^= CMD_LOAD_ADDRESS;
					stkChecksum ^= 0x00;
					stkChecksum ^= ((hexAddress>>16) & 0xFF);
					stkChecksum ^= ((hexAddress>>8) & 0xFF);
					stkChecksum ^= (hexAddress&0xFF);
					
					uint8_t tempS[] = {CMD_LOAD_ADDRESS, 0x00, ((hexAddress>>16) & 0xFF)\
						 ,((hexAddress>>8) & 0xFF), (hexAddress & 0xFF), stkChecksum};
					
// 					for(uint8_t i=0; i<(sizeof(tempS)/sizeof(tempS[0])); ++i){
// 						if(write_to_eeprom(_address, tempS[i])){
// 							Serial.println("WRITE FAIL");
// 							return 1;
// 						}
// 						_address++;
// 					}
					
					if(write_to_memory_array(_address, tempS, (sizeof(tempS)/sizeof(tempS[0]))) != 0){
						Serial.println("WRITE FAIL");
						return 1;
					}
					
					_address += (sizeof(tempS)/sizeof(tempS[0]));
					
					stkMSGState = CMD_ENTER_PROGMODE_ISP;
					hexParseState = 20;
					stkSequenceNumber++;
					break;
				}
				
				case CMD_ENTER_PROGMODE_ISP:{
					//Serial.println(stkSequenceNumber);
					DEBUG_PRINT("ENTER PROG MODE");
					stkChecksum = 0;
					if(start_stk_msg(_address, stkSequenceNumber, 1, &stkChecksum)){
						Serial.println("Write Data Fail");
						return 1;
					}
					_address += 5;
					
					stkChecksum ^= CMD_ENTER_PROGMODE_ISP;
					
					uint8_t tempS[] = {CMD_ENTER_PROGMODE_ISP, stkChecksum};
						
// 					for(uint8_t i=0; i<sizeof(tempS); ++i){
// 						if(write_to_eeprom(_address, tempS[i])){
// 							Serial.println("WRITE FAIL");
// 							return 1;
// 						}
// 						_address++;
// 					}

					if(write_to_memory_array(_address, tempS, (sizeof(tempS)/sizeof(tempS[0]))) != 0){
						Serial.println("WRITE FAIL");
						return 1;
					}
					
					_address += (sizeof(tempS)/sizeof(tempS[0]));
					
					stkMSGState = CMD_PROGRAM_FLASH_ISP;
					stkSequenceNumber++;
					
					inProgMode = 1;
					
					break;
				}
				
				case CMD_PROGRAM_FLASH_ISP:{
					stkChecksum = 0;
					uint16_t tempByteCount = 0;
					tempByteCount = hexByteCount + 10;
					//Serial.println("Program Flash ISP");
					if(start_stk_msg(_address, stkSequenceNumber, tempByteCount, &stkChecksum)){
						Serial.println("Write Data Fail");
						return 1;
					}
					_address += 5;
					
					stkChecksum ^= CMD_PROGRAM_FLASH_ISP;
					stkChecksum ^= 0x00;
					stkChecksum ^= tempByteCount;	
					
					uint8_t tempS[tempByteCount];
					
					tempS[0] = CMD_PROGRAM_FLASH_ISP;
					tempS[1] = (tempByteCount >> 8) & 0xFF;
					tempS[2] = tempByteCount;
					tempS[3] = 0x00;
					tempS[4] = 0x00;
					tempS[5] = 0x00;
					tempS[6] = 0x00;
					tempS[7] = 0x00;
					tempS[8] = 0x00;
					tempS[9] = 0x00;
					
					uint8_t lowByteCounter = 10;
					uint8_t highByteCounter = 11;
					
					for(int i=0; i<hexByteCount; ++i){
						tempS[i+10] = hexData[i];
						stkChecksum ^= hexData[i];
					}		
					
// 					for(uint8_t i=0; i<(sizeof(tempS)/sizeof(tempS[0])); ++i){
// 						if(write_to_eeprom(_address, tempS[i])){
// 							Serial.println("WRITE FAIL");
// 							return 1;
// 						}
// 						_address++;
// 					}

					if(write_to_memory_array(_address, tempS, (sizeof(tempS)/sizeof(tempS[0]))) != 0){
						Serial.println("WRITE FAIL");
						return 1;
					}
					
					_address += (sizeof(tempS)/sizeof(tempS[0]));
					
					
					if(write_to_memory_array(_address, &stkChecksum, 1) != 0){
						Serial.println("WRITE FAIL");
						return 1;
					}
					_address++;
					
					stkMSGState = 0;
					writeToEEPROM = 0;
					hexParseState = HEX_START_LINE;
					stkSequenceNumber++;
					
					break;
				}
				
				case CMD_LEAVE_PROGMODE_ISP:{
					Serial.println("LEAVE PROG MODE");
					stkChecksum = 0;
					if(start_stk_msg(_address, stkSequenceNumber, 1, &stkChecksum)){
						Serial.println("Write Data Fail");
						return 1;
					}
					_address += 5;
					
					stkChecksum ^= CMD_LEAVE_PROGMODE_ISP;
					
					uint8_t tempS[] = {CMD_LEAVE_PROGMODE_ISP, stkChecksum};
						
// 					for(uint8_t i=0; i<(sizeof(tempS)/sizeof(tempS[0])); ++i){
// 						if(write_to_eeprom(_address, tempS[i])){
// 							Serial.println("WRITE FAIL");
// 							return 1;
// 						}
// 						_address++;
// 					}

					if(write_to_memory_array(_address, tempS, (sizeof(tempS)/sizeof(tempS[0]))) != 0){
						Serial.println("WRITE FAIL");
						return 1;
					}
					
					_address += (sizeof(tempS)/sizeof(tempS[0]));
					
					return 0;
				} 
			}
		}
	}
	return 1;
}

//Get 4 bits of hex from ASCII
uint8_t get_hex_4(uint8_t i)
{
	if      (i >= 0x61 && i <= 0x66) return i - 0x61 + 10;
	else if (i >= 0x41 && i <= 0x46) return i - 0x41 + 10;
	else if (i >= 0x30 && i <= 0x39) return i - 0x30;
	else return 0;

}

//Write Starting sequence to EEPROM
uint8_t start_stk_msg(int a, int seqNumber, int messageSize, uint8_t *stkCheckSum)
{
	uint8_t startCMDS[] = {MSG_START, seqNumber, ((messageSize>>8) & 0xFF), (messageSize & 0xFF) , TOKEN};
	
	*stkCheckSum ^= MSG_START;
	*stkCheckSum ^= seqNumber;
	*stkCheckSum ^= ((messageSize>>8) & 0xFF);
	*stkCheckSum ^= (messageSize & 0xFF);
	*stkCheckSum ^= TOKEN;
	
	
// 	for(uint8_t i=0; i<(sizeof(startCMDS)/sizeof(startCMDS[0])); ++i){
// 		if(write_to_eeprom(a, startCMDS[i])){
// 			Serial.println("WRITE FAIL");
// 			return 1;
// 		}
// 		a++;
// 	}

	if(write_to_memory_array(a, startCMDS, (sizeof(startCMDS)/sizeof(startCMDS[0]))) != 0){
		return 1;
	}
	
	return 0;
}

//calculates the number of parts to download
uint8_t get_download_byte_chunks(uint32_t totalSize, uint16_t chunkSize)
{
	if((totalSize%chunkSize) == 0){
		return totalSize/chunkSize;
	}
	else{
		return (totalSize/chunkSize)+1;
	}
}


HttpClient http(client, otaServer, 80);

uint16_t contentLength = 0;
uint16_t chunkNumber = get_download_byte_chunks(fileSize, HTTP_BYTE_RANGE);
uint32_t chunkCounter = 0;


//Returns downloaded byte
uint8_t download_byte()
{
	if(!http.available() && (contentLength <= 0)){//if no data is available, download next chunck
		chunkCounter++;
		uint32_t tempRangeEnd = (((chunkCounter*HTTP_BYTE_RANGE)+HTTP_BYTE_RANGE)-1);
		String rangeEnd = String(tempRangeEnd);
		
		DEBUG_PRINT(String(tempRangeEnd));
		
		if(fileSize < tempRangeEnd){
			rangeEnd = String(" ");
		}
		
		uint8_t retries = 0;
		int err = send_http_get_range_request(otaServer, otaResource, 80, String((chunkCounter*HTTP_BYTE_RANGE)), rangeEnd);
		while(err != 0){
			if(retries < 3){
				err = send_http_get_range_request(otaServer, otaResource, 80, String((chunkCounter*HTTP_BYTE_RANGE)), rangeEnd);
				retries++;
				continue;
			}
			delay(1000);
			//
			return 255;
		}
		
	}
	
	uint8_t c = http.read();
	Serial.println(c, HEX);
	contentLength--;
	return c;
}

//Main loop
void ota_handler()
{
	wdt_disable();
	
	uint8_t retries = 0;
	while(send_http_head_request(otaServer, otaResource, 80) != 0){//There was an error communicating with server. abort OTA
		if(retries < 4){
			return;
		}
		_delay_ms(100);
		retries++;
	}
	
	init_spi_eeprom();
	
	uint8_t err = send_http_get_range_request(otaServer, otaResource, 80, String("0"), String((HTTP_BYTE_RANGE-1)));//Initial http get Request
	
	if(err == 0){//No error, proceed to write data to EEPROM
		uint8_t err = write_stk_msg_to_eeprom();
		if(!err){
			DEBUG_PRINT("REBOOT");
			EEPROM.write(2, 1);
			wdt_enable(WDTO_8S);
			while(1){;}
		}
	}
	http.stop();
	wdt_enable(WDTO_8S);
}

//Head request used to get the filesize; file size is used to calculate the number of byte chunks to download
uint8_t send_http_head_request(const char *server, const char *resource, uint16_t port)
{
	DEBUG_PRINT("Sending HEAD Request...");
	DEBUG_PRINT(String(server));
	http.connectionKeepAlive();
	
	http.beginRequest();
	http.startRequest(resource, "HEAD");
	http.sendHeader("Range", " ");
	http.endRequest();
	
	int status = http.responseStatusCode();
	DEBUG_PRINT(String(status));
	if(status <= 0){
		if(status == -3){
			DEBUG_PRINT("Timeout");
		}
		return 2;
	}
	
	DEBUG_PRINT("OK");
	
	while(http.headerAvailable()){
		DEBUG_PRINT(String(http.readHeaderName()) + ":" + String(http.readHeaderValue()));
	}
	
	if(status == 200){
		fileSize = http.contentLength();
		DEBUG_PRINT("\nFile Size : " + String(fileSize));
		return 0;
	}
	
	return 1;
}

//HTTP Get byte range request. Allows for downloading large files
uint8_t send_http_get_range_request(const char *server, const char *resource, uint16_t port, String rangeStart, String rangeEnd)
{
	DEBUG_PRINT("Getting Bytes:" + String(rangeStart) + "-" + String(rangeEnd));
	
	http.beginRequest();
	http.startRequest(resource, "GET");
	http.sendHeader("Range", ("bytes=" + rangeStart + String("-") + rangeEnd));
	http.endRequest();
	
	int status = http.responseStatusCode();
	if(status <= 0){
		return 2;
	}
	
	DEBUG_PRINT("Ok");
	
	while(http.headerAvailable()){
		DEBUG_PRINT(String(http.readHeaderName()) + ":" + String(http.readHeaderValue()));
	}
	
	if(status == 206){
		contentLength = http.contentLength();
		//DEBUG_PRINT(String(contentLength));
		return 0;
	}
	
	return 1;
}
