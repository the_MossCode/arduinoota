/*
 * m95_spi.cpp
 *
 * Created: 19/10/2018 19:54:49
 *  Author: Collo
 */ 
#include "ota.h"

void init_spi_eeprom()
{
	DDR_SPI = SPCR = 0x00;
	
	DDR_SPI |= (1<<EEPROM_CS) | (1<<EEPROM_MOSI) | (1<<EEPROM_SCK); //Set SS MOSI and SCK as outputs
	EEPROM_SS(1);
	SPCR |= (1<<SPE) | (1<<MSTR) | (1<<SPR0);//spi mode0, msb first
	
	DEBUG_PRINT("SPI Initialised..");
	_delay_ms(50);
}

uint8_t spi_send_byte(uint8_t b)
{
	SPDR = b;
	while(!(SPSR & _BV(SPIF)));
	
	return SPDR;
}

void set_write_enable_latch()
{
	spi_send_byte(WRITE_ENABLE);
}

void clear_write_enable_latch()
{
	spi_send_byte(WRITE_DISABLE);
}

uint8_t read_status_register()
{
	return spi_send_byte(READ_STATUS_REGISTER);
}

uint8_t write_to_memory_array(uint32_t a, uint8_t *d, uint16_t l)
{
	uint8_t eepromWriteState = EEPROM_STATE_IDLE;
	EEPROM_SS(1);
	
	int i =0;
	Serial.println("Write");
	
	while(i < l){
		switch(eepromWriteState){
			case EEPROM_STATE_IDLE:{
				uint32_t timeout = millis();
				
				EEPROM_SS(0);
				
				for(uint8_t i=0; i<5; ++i){//Sometimes the status reads 0 on first read after Selecting 
					read_status_register();
				}
				
				while(read_status_register() & 0x01){//Write in progress
					Serial.println("Write in Progress - " + String(read_status_register()));
					if((millis() - timeout) > 500){
						Serial.println("timeout");
						return 1;
					}
				}
				EEPROM_SS(1);
				_delay_ms(1);
				
				EEPROM_SS(0);
				set_write_enable_latch();
				EEPROM_SS(1);
				_delay_ms(1);
				
				EEPROM_SS(0);
				for(uint8_t i=0; i<5; ++i){
					read_status_register();
				}
				
				while(!(read_status_register() & 0x02)){//Write enable latch not set
					Serial.println(read_status_register());
					if((millis() - timeout) > 500){
						Serial.println("timeout");
						return 1;
					}
				}
				EEPROM_SS(1);
				_delay_ms(1);
				
				eepromWriteState = EEPROM_STATE_LOAD_ADDRESS;
				break;
			}
			
			case EEPROM_STATE_LOAD_ADDRESS:{
				EEPROM_SS(0);
				spi_send_byte(WRITE_TO_MEMORY_ARRAY);
				spi_send_byte((uint8_t)((a>>16) & 0xff));
				spi_send_byte((uint8_t)((a>>8) & 0xff));
				spi_send_byte((uint8_t) (a & 0xff));
				
				eepromWriteState = EEPROM_STATE_WRITE;
				break;
			}
			
			case EEPROM_STATE_WRITE:{
				spi_send_byte(d[i++]);
				if(i == l){
					EEPROM_SS(1);
					_delay_ms(1);
					break;
				}

				if((a++ & 0xff) == 255){
					Serial.println("Page Boundary");
					Serial.println(a);
					EEPROM_SS(1);
					_delay_ms(1);
					
					eepromWriteState = EEPROM_STATE_IDLE;
				}
					
					break;
			}
		}
	}
	
	EEPROM_SS(1);
	_delay_ms(1);
	
	return 0; //No Errors
}

uint16_t read_from_memory_array(uint32_t a, uint8_t *d, uint16_t l)
{
	uint8_t eepromReadState = EEPROM_STATE_IDLE;
	EEPROM_SS(1);
	
	int i = 0;
	
	Serial.println("Read");
	
	while(i < l){
		switch(eepromReadState){
			case EEPROM_STATE_IDLE:{
				EEPROM_SS(0);
				
				for(uint8_t i=0; i<5; ++i){
					read_status_register();
				}
				
				
				uint32_t timeout = millis();
				while((read_status_register() & 0x01)){//Write in Progress
					if((millis() - timeout) > 500){
						return 0;
					}
				}
				EEPROM_SS(1);
				_delay_ms(1);
				
				eepromReadState = EEPROM_STATE_LOAD_ADDRESS;
				break;
			}
			
			case EEPROM_STATE_LOAD_ADDRESS:{
				EEPROM_SS(0);
				spi_send_byte(READ_FROM_MEMORY_ARRAY);
				spi_send_byte((uint8_t)((a>>16) & 0xff));
				spi_send_byte((uint8_t)((a>>8) & 0xff));
				spi_send_byte((uint8_t) (a & 0xff));
				
				eepromReadState = EEPROM_STATE_READ;
				break;
			}
			
			case EEPROM_STATE_READ:{
				uint8_t k = spi_send_byte(0xFF);
				d[i++] = k;
				break;
			}
		}
	}
	
	EEPROM_SS(1);
	_delay_ms(1);
	
	return i;
}
