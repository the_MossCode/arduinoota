/*
 * ota.h
 *
 * Created: 20/10/2018 19:03:26
 *  Author: Collo
 */ 


#ifndef OTA_H_
#define OTA_H_

#define TINY_GSM_MODEM_SIM800
#define DEBUG 1

#include "HttpClient.h"
#include "m95_spi.h"
#include "DebugUtils.h"
#include <eeprom.h>
#include <avr/wdt.h>
#include <TinyGsmClient.h>


#endif /* OTA_H_ */