/*
 * m95_spi.h
 *
 * Created: 19/10/2018 19:53:47
 *  Author: Collo
 */ 


#ifndef M95_SPI_H_
#define M95_SPI_H_


#include <sys/types.h>
//SPI EEPROM Definitions
#define	WRITE_ENABLE					0x06
#define	WRITE_DISABLE					0x04
#define	READ_STATUS_REGISTER			0x05
#define	WRITE_STATUS_REGISTER			0x01
#define	READ_FROM_MEMORY_ARRAY			0x03
#define	WRITE_TO_MEMORY_ARRAY			0x02
#define	READ_IDENTIFICATION_PAGE		0x83
#define	WRITE_IDENTIFICATION_PAGE		0x82

#define EEPROM_CS						PINB0
#define EEPROM_SCK						PINB1
#define EEPROM_MOSI						PINB2
#define EEPROM_MISO						PINB3
#define DDR_SPI							DDRB

#define EEPROM_SS(x)					(((x)==0)?(PORTB &= ~(1<<EEPROM_CS)):(PORTB |= (1<<EEPROM_CS)))

#define PAGE_SIZE						256

#define EEPROM_STATE_IDLE				0
#define EEPROM_STATE_LOAD_ADDRESS		1
#define EEPROM_STATE_WRITE				2
#define EEPROM_STATE_READ				3

void init_spi_eeprom(void);
uint8_t spi_send_byte(uint8_t);
void set_write_enable_latch(void);
void clear_write_enable_latch(void);
uint8_t read_status_register(void);
uint8_t write_to_memory_array(uint32_t, uint8_t*, uint16_t);
uint16_t read_from_memory_array(uint32_t, uint8_t*, uint16_t);

#endif /* M95_SPI_H_ */